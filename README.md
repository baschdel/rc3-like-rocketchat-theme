# rc3-like-rocketchat-theme

Css mods to make the rc3 rocketchat look more like rc3

## Screenshots

![A screenshot showing the homescreen on desktop](screenshots/rc3_2021/home_desktop.png)
![A screenshot showing the chat on desktop](screenshots/rc3_2021/chat_desktop.png)
![A screenshot showing the preferences on desktop](screenshots/rc3_2021/settings_desktop.png)

